package richtercloud.primefaces.tree.recursive;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

@Named
@ViewScoped
public class BackingBean0 implements Serializable {
    private MapEntity mapEntity = createMapEntity();

    public MapEntity getMapEntity() {
        return mapEntity;
    }

    public void setMapEntity(MapEntity mapEntity) {
        this.mapEntity = mapEntity;
    }

    public TreeNode createNodes(Map<String, TreeEntity> map) {
        TreeNode root = new DefaultTreeNode(new MyTreeNodeData("",
                        null
                ), //data (values are never used)
                null //parent
        );
        createNodesRecurse(map,
                root);
        return root;
    }

    public void createNodesRecurse(Map<String, TreeEntity> map,
            TreeNode root) {
        for(String key : map.keySet()) {
            TreeEntity treeEntity = map.get(key);
            if(treeEntity.getValue() instanceof String) {
                new DefaultTreeNode(new MyTreeNodeData(key,
                                treeEntity),
                        root //parent
                );
            }else if(treeEntity.getValue() instanceof Map) {
                Map<String, TreeEntity> map0 = (Map<String, TreeEntity>) treeEntity.getValue();
                TreeNode root0 = new DefaultTreeNode(new MyTreeNodeData("recurse",
                                null),
                        root //parent
                );
                createNodesRecurse(map0,
                        root0);
            }else {
                throw new IllegalArgumentException();
            }
        }
    }

    private MapEntity createMapEntity() {
        Map<String, TreeEntity> map = new HashMap<>();
        map.put("0", new TreeEntity("initial value"));
        Map<String, Object> branchEntityMap = new HashMap<>();
        branchEntityMap.put("1a", new TreeEntity("branch value"));
        map.put("1", new TreeEntity(branchEntityMap));
        return new MapEntity(map);
    }
}
