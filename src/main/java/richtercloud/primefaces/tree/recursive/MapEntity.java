package richtercloud.primefaces.tree.recursive;

import java.util.Map;

public class MapEntity {
    private Map<String, TreeEntity> map;

    public MapEntity() {
    }

    public MapEntity(Map<String, TreeEntity> map) {
        this.map = map;
    }

    public Map<String, TreeEntity> getMap() {
        return map;
    }

    public void setMap(Map<String, TreeEntity> map) {
        this.map = map;
    }
}
