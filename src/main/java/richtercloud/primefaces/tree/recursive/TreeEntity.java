package richtercloud.primefaces.tree.recursive;

public class TreeEntity {
    private Object value;

    public TreeEntity() {
    }

    public TreeEntity(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        System.out.println("value: "+value);
        this.value = value;
    }
}
